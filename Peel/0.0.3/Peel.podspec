#
#  Be sure to run `pod spec lint Peel.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "Peel"
  s.version      = "0.0.3"
  s.summary      = "iOS Library for Peel to monitor user locations using iBeacons"

  

  s.homepage     = "http://turnsolutions.net"
  s.license      = {
    :type => 'Copyright',
    :text => <<-LICENSE
      Copyright 2013-2015 Turn Solutions, Inc. All rights reserved.
      LICENSE
  }

  s.author             = { "Hiteshwar Vadlamudi" => "hitesh.vadlamudi@sparity.com" }

  s.platform     = :ios
  
  s.source       = { :git => "https://bitbucket.org/hvadlamudi/peelpod.git", :tag => "0.0.3" }
  s.source_files  = 'Peel/Peel.framework/Headers/*.h'
  s.vendored_frameworks = 'Peel/Peel.framework'
  s.ios.deployment_target = '8.0'

  s.dependency "Parse"
  s.dependency "ParseFacebookUtilsV4"
  
  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
